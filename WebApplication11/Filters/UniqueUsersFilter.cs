﻿using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc;

namespace WebApplication11.Filters
{
    public class UniqueUsersFilter : IActionFilter
    {
        private const string UserIdCookieName = "UserId";
        private static HashSet<string> uniqueUsers = new HashSet<string>();

        public void OnActionExecuting(ActionExecutingContext context)
        {

            if (context.HttpContext.Request.Cookies.TryGetValue(UserIdCookieName, out var userId))
            {
                uniqueUsers.Add(userId);
            }
            else
            {
                userId = Guid.NewGuid().ToString();
                uniqueUsers.Add(userId);

                context.HttpContext.Response.Cookies.Append(UserIdCookieName, userId);
            }
        }

        public void OnActionExecuted(ActionExecutedContext context)
        {
            File.WriteAllText("users_log.txt", $"unique users: {uniqueUsers.Count}");
        }

    }
}
