﻿using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.IO;
    using System.Text;
namespace WebApplication11.Filters
{

    public class LogActionFilter : Attribute, IActionFilter
    {

        public void OnActionExecuting(ActionExecutingContext context)
        {
            var logMessage = $"Method: {context.ActionDescriptor.DisplayName}, Time: {DateTime.Now}\n";
            File.AppendAllText(Path.Combine(Directory.GetCurrentDirectory(), "logs.txt"), logMessage, Encoding.UTF8);
        }

        public void OnActionExecuted(ActionExecutedContext context)
        {
           
        }
    }

}
